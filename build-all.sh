# Check if /tmp exsits
if [ -d "tmp/" ]; then
    echo "Removing old tmp folder"
    rm -rf tmp/
    mkdir tmp/
else
    echo "Creating tmp folder"
    mkdir tmp/
fi

# Build all packages in all subdirectories
for dir in */; do
    cd "$dir"
    if [ -f "PKGBUILD" ]; then
        echo "Building $dir"
        makepkg -f

        echo "Copying built $dir to /tmp"
        cp -r *.pkg.tar.zst ../tmp/
    fi
    cd ..
done